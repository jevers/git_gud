import os
import numpy as np

import sys
print(sys.executable)

os.makedirs('models', exist_ok=True)


model_name = 'amazing_model'
ran_num = np.random.randint(1000)
with open(f'./models/{model_name}_{ran_num}.pkl', 'a') as f:
	f.write('model')