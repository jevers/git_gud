def jochems_function():
	"""Jochem can write his own function here"""
	print('Jochem')


def roelants_function():
	"""Roelant can write his own function here"""
	# Haha this is my function
	return 1

def jochem_and_roelant_shared_function():
	"""Both Roelant and Jochem will alter this function"""
	print("Jochem heeft dit aangepast")